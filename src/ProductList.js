import React, {useState, useEffect} from 'react'
import styles from './ProductList.module.css'
import {Link} from 'react-router-dom'
import Title from './Title'
import QuantityBtn from './QuantityBtn'


export default function ProductList() {
    /*let productList = [
        {"id": 1,"name": "相機", "price": 1000, "image": "camera.jpg", "description": "拍照"},
        {"id": 2,"name": "電話", "price": 5000, "image": "phone.jpg", "description": "打電話"},
    ]*/
    
    //let product = "產品"
    
    let [productList, setProductList] = useState([])

    useEffect(()=>{
        /*fetch("https://wlchengivan.gitlab.io/ivanchengwebsite/hktvmall.json")
        .then(response => response.json())
        .then(data => setProductList(data))*/

        fetch("http://localhost:3000/json/hktvmall.json")
        .then(response => response.json())
        .then(data => setProductList(data))
        
    },[] //空array=只在第1次render時觸發, 沒參數=每次render時觸發, 有變數=第1次render+變數改變時觸發
    )


  return (
    <>
        <Title mainTitle="產品名單"/>
        <div className="container">
            {productList.map((product) => {
                return(

                    <div className="containerItem" key={product.id}>
                        <Link to={"/product/" + product.id}>
                            <img height="250" width="250" src={product.image}></img><br/>
                        </Link>
                        <div className="productName">
                            {product.name}<br/>
                            單價 {parseFloat(product.price).toFixed(2)} 元<br/>
                            <QuantityBtn productInfo={product}/>
                        </div>
                    </div>
                )
            })}
        </div>
    </>
  )
}
