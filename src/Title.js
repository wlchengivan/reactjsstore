import React from 'react'

export default function Title({mainTitle}) {
  return (
    <div>
      <h1 style={{backgroundColor:"green", borderBottom:'5px solid red'}}>
        {mainTitle}
      </h1>
    </div>
  )
}
