import CheckOut from "./CheckOut";
import NotFound404 from "./NotFound404";
import ProductDetail from "./ProductDetail";
import ProductList from "./ProductList";
import { BrowserRouter, Routes, Route, Link} from 'react-router-dom'
import { useState } from "react";
import { CartContext } from "./CardContext";

function App() {
  const [cartItems, setCartItems] = useState([])

  return (
    <BrowserRouter>

      <CartContext.Provider value={{cartItems, setCartItems}}>

        <nav>
          <Link to="/">首頁</Link>
          <Link to="/checkout">結賬</Link>
          </nav>

        <Routes>
          <Route path="/" element={<ProductList/>}/>
          <Route path="/checkout" element={<CheckOut/>}/>


          <Route path="/product" element={<ProductDetail/>}>
            <Route path=":id" element={<ProductDetail/>}/>
          </Route>


          <Route path="*" element={<NotFound404/>}/>
        </Routes>

      </CartContext.Provider>
    </BrowserRouter>
  );
}

export default App;
