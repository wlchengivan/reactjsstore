import React, { useContext, useState, useEffect } from 'react'
import {useParams, Link} from 'react-router-dom'
import Title from './Title'
import QuantityBtn from './QuantityBtn'
import { CartContext } from './CardContext'

export default function ProductDetail() {
    let {cartItems} = useContext(CartContext)
    let params = useParams()
    let [productDetail, setProductDetail] = useState(null)

    useEffect(()=>{
        fetch("http://localhost:3000/json/hktvmall.json")
        .then(response => response.json())
        .then(data => {
            let productInfo = data.find((element)=>{
                return element.id === parseInt(params.id)
            })
            setProductDetail(productInfo)
        })
    },[] //空array=只在第1次render時觸發, 沒參數=每次render時觸發, 有變數=第1次render+變數改變時觸發
    )

    return (
        <div className="ProductDetail">
            {
                productDetail && <React.Fragment className="ProductDetail">
                    <Title mainTitle={productDetail.name}/>
                    <img width="250" height="250" src={productDetail.image}></img><br/>
                    產品名稱 : {productDetail.name}<br/>
                    價錢 : {parseFloat(productDetail.price).toFixed(2)} 元<br/>
                    產品詳情 : <br/>{productDetail.description}

                    <QuantityBtn productInfo={productDetail}/>
                    <Link to={"/"}>返回</Link>
                </React.Fragment>
            }
        </div>
  )
}
