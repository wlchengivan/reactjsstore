import React, { useContext } from 'react'
import Title from './Title'
import { Link } from 'react-router-dom'
import QuantityBtn from './QuantityBtn'
import { CartContext } from './CardContext'

export default function CheckOut() {

    let {cartItems} = useContext(CartContext)

    let cartEmpty = cartItems.length <= 0? true : false

    let grandTotal = cartItems.reduce((total, product)=>{
      return total += product.price * product.quantity  
    }, 0)

    const freeShippingPrice = 99

  return (
    <>
        <Title mainTitle="結賬"/>

        {
            cartEmpty && <div className="nothingInCart">
                購物車為空<br/>
                <Link to="/">返回產品列表</Link>
            </div>
        }


        {
            !cartEmpty && <>
                <div className="cartSection">
                        <table className="checkoutTable">
                            <tbody>
                                {
                                    cartItems.map(product=>(
                                        <tr key={product.id}>
                                            <td>
                                                <Link to={'/product/'+product.id}>
                                                <img height="250" width="250" src={product.image} alt={product.name}/>
                                                </Link>
                                            </td>
                                            <td>
                                                <p>名稱 : {product.name}</p>
                                                <p>售價 : {product.price}元</p>
                                                <p>描述 : {product.description}</p>
                                            </td>
                                            <td width="200">
                                                <QuantityBtn productInfo={product} />
                                            </td>
                                            <td>
                                                <div className="productSubTotal">
                                                    ${parseFloat(product.price*product.quantity).toFixed(2)}
                                                </div>
                                            </td>
                                        </tr>

                                    ))
                                }
                            </tbody>
                        </table>
                    </div>

                <div className="checkoutSection" id="checkOutSection">
                    <div  className="grandTotal">總計</div>
                    <div  className="grandTotal">{parseFloat(grandTotal).toFixed(2)} 元 </div>

                    {
                        grandTotal >= freeShippingPrice ?
                        <div className="freeShipping">免費送貨</div> :
                        <div className="noShipping">買滿 {freeShippingPrice} 元免費送費(還欠{parseFloat(freeShippingPrice - grandTotal).toFixed(2)} 元)</div>
                    }
                </div>
            </>
        }
    </>
    
  )
}
